import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { NgForm} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {
 @ViewChild('f') form!: NgForm;
 cript: Object = '';
 noCript: Object = '';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private http: HttpClient) {}

  onSubmitCript() {
    const password = this.form.value.password;
    const text = this.form.value.text;
    this.http.get(`http://localhost:8000/encode/${password}/${text}`).subscribe(cript => {
      this.cript = cript;
    });
  };

  onSubmitNotCript() {
    const password = this.form.value.password;
    this.http.get(`http://localhost:8000/decode/${password}/${this.cript}`).subscribe(noCript => {
      this.noCript = noCript;
    });
  };
}
