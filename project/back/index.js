const express = require('express');
const app = express();
const cors = require('cors');
const port = 8000;
const data = [];
const changeData = [];
app.use(cors({origin: 'http://localhost:4200'}));

const Vigenere = require('caesar-salad').Vigenere;

const encode = (letter) => {
  return Vigenere.Cipher('password').crypt(`${letter}`);
};

const decode = (encode) => {
  return Vigenere.Decipher('password').crypt(`${encode}`);
};

app.get('/encode/password/:letter', (req, res) => {
  data.push(encode(req.params.letter));
  res.send(data);
});

app.get(`/decode/password/:encode`, (req, res) => {
  changeData.push(decode(req.params.encode));
  res.send(changeData);
});

app.listen(port, () => {
  console.log('We are live on ' + port);
});

